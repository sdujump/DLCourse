# DLCourse

DL Course

requirements 1:        conda install pytorch=0.3.1 -c soumith 

(with nvidia gpu:   conda install pytorch=0.3.1 cuda80 -c soumith)


requirements 2:        conda install torchvision -c pytorch


camera real time style transfer:

python camera_demo.py demo --model models/21styles.model --cuda=0 --demo-size=100 

offline image style transfer:

python main.py eval --content-image images/content/venice-boat.jpg --style-image images/21styles/candy.jpg --model models/21styles.model --content-size 1024 --cuda=0