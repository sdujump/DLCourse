import sys
import network
import mnist_loader
import numpy as np
import cv2

def main():
    print "Training"
    training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
    net = network.Network([784, 30, 10])
    net.SGD(training_data, 30, 10, 3.0, test_data=test_data)
    out_w = np.resize(net.weights[0],[30,28,28])
    norm_out_w = norm_save(out_w)
    

def norm_save(input_data):
    for i in range(np.shape(input_data)[0]):
        input_data[i]=(input_data[i]-input_data[i].min())*255.0/(input_data[i].max()-input_data[i].min())
        cv2.imwrite(str(i)+".jpg",input_data[i].astype(int))
    return input_data

if __name__ == "__main__":
    sys.exit(main())